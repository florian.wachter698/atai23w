package com.visualization;

import com.visualization.domain.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TeamServerVisualizationImpl {
    private VisualizationGameStateImpl gameState;
    private long timeStamp;
}
