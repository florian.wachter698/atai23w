package com.visualization.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GamePointsData {
    private String gameName;
    private long pointsCyan;
    private long pointMagenta;
}
