package com.grips.scheduler.asp;

import com.grips.persistence.dao.AtomDao;
import com.grips.persistence.domain.Atom;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class FakeAtomDao implements AtomDao {
    Map<Long, Atom> data;
    Random nextId;
    public FakeAtomDao() {
        data = new HashMap<>();
        nextId = new Random();
    }
    @Override
    public List<Atom> findByNameAndAttributes(String name, String attributes) {
        return data.values().stream()
                .filter(x -> x.getName().equals(name) && x.getAttributes().equals(attributes))
                .collect(Collectors.toList());
    }

    @Override
    public List<Atom> findByName(String name) {
        return data.values().stream()
                .filter(x -> x.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public <S extends Atom> S save(S entity) {
        if (entity.getId() == 0) {
            entity.setId(nextId.nextLong());
        }
        this.data.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public <S extends Atom> Iterable<S> saveAll(Iterable<S> entities) {
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::save)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Atom> findById(Long aLong) {
        return Optional.ofNullable(this.data.get(aLong));
    }

    @Override
    public boolean existsById(Long aLong) {
        return data.containsKey(aLong);
    }

    @Override
    public Iterable<Atom> findAll() {
        return data.values();
    }

    @Override
    public Iterable<Atom> findAllById(Iterable<Long> longs) {
        List<Long> ids = new ArrayList<>();
        longs.forEach(ids::add);
        return data.values().stream()
                .filter(ids::contains)
                .collect(Collectors.toList());
    }

    @Override
    public long count() {
        return data.size();
    }

    @Override
    public void deleteById(Long aLong) {
        this.data.remove(aLong);
    }

    @Override
    public void delete(Atom entity) {
        this.data.remove(entity.getId());
    }

    @Override
    public void deleteAll(Iterable<? extends Atom> entities) {
        entities.forEach(this::delete);
    }

    @Override
    public void deleteAll() {

    }
}
