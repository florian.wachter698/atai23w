;Header and description

(define (domain Robots)

;remove requirements that are not needed
(:requirements :strips :typing :durative-actions :numeric-fluents)

(:types ;todo: enumerate types and their hierarchy here, e.g. car truck bus - vehicle
    robot - object
    machine - object
     
    product - object

    ringstation - machine
    capstation - machine
    deliverstation - machine
    basestation - machine 
    start - machine    

    base - product
    ring - product
    cap - product

    standardbase - base
    blackbase - standardbase 
    redbase - standardbase
    
    greenring - ring
    yellowring - ring
    bluering - ring
    orangering - ring
    
    greycap - cap
    blackcap - cap 
    capped_base - base

  

    side - object
)       

; un-comment following line if constants are needed
;(:constants )

(:predicates ;todo: define predicates here

    (robot_without_product ?r - robot)
    (robot_with_product ?r - robot ?p - product)
    (robot_at_machine_side ?r - robot ?m - machine  ?s - side)
    (machine_with_product ?m - machine ?p - product)
    (machine_without_product ?m - machine)


    (ringstation_has_product ?m - ringstation ?p - product)
    (capstation_has_product ?m - capstation ?p - product)
    (is_input ?s - side)
    (is_output ?s - side)
    

    (has_capstation_buffer ?m - machine ?p - product)
    (cap_and_base_connected ?cap - product ?capped_base - product)

    (is_something_below_product ?p - product)
    (is_something_above_product ?p - product)
    (is_free_above ?p - product)
    (is_free_below ?p - product)

    (is_no_robot_at_side ?s - side)

    (stacked ?top - product ?bottom - product ?base - base)
    (side_connected_to_machine ?m - machine ?s - side)
)   


(:functions ;todo: define numeric functions here
    (bases_on_ringsation ?m - ringstation)
    (cost_of_ring ?r - ring)
)

(:action get
    :parameters (?r - robot ?m - machine ?p - base ?s - side)
    :precondition  
    (and
        (is_output ?s)  
        (robot_without_product ?r) 
        (machine_with_product ?m ?p)
        (robot_at_machine_side ?r ?m ?s)
    )
    :effect
    (and    
        (machine_without_product ?m) 
        (robot_with_product ?r ?p) 
        (not (robot_without_product ?r)) 
        (not (machine_with_product ?m ?p))
    )
)

(:durative-action move
    :parameters (?r - robot ?target_machine - machine ?target_side - side  ?previous_machine - machine ?previous_side - side)
    :duration (= ?duration 10)
    :condition 
    (and 
        (at start 
            (and 
                (robot_at_machine_side ?r ?previous_machine ?previous_side)
               
            )               
        )
        (over all 
            (and
                (side_connected_to_machine ?target_machine ?target_side)
            )
        )
        (at end   
            (and 
                        
                    (is_no_robot_at_side ?target_side)
            )
        )
    )
    :effect 
    (and 
        (at start
            (and 
                (is_no_robot_at_side ?previous_side)
                (not(robot_at_machine_side ?r ?previous_machine  ?previous_side))
            )
        )
        (at end 
            (and 
                (robot_at_machine_side ?r ?target_machine ?target_side) 
                (not(is_no_robot_at_side ?target_side))
            )
        )
    )
)


(:action buffer
    :parameters (?r - robot ?m - capstation ?s - side ?p - cap ?capped_base - capped_base)
    :precondition   
    (and
        (robot_at_machine_side ?r ?m ?s)
        (is_input ?s)
        (robot_without_product ?r)
        (machine_without_product ?m)
        (cap_and_base_connected ?p ?capped_base)
        (capstation_has_product ?m ?p)
    )

    :effect 
    (and
        (is_free_above ?capped_base)
        (is_free_below ?p)
        (has_capstation_buffer ?m ?p)
        (machine_with_product ?m ?capped_base)
        (not(machine_without_product ?m))
        (not(cap_and_base_connected ?p ?capped_base))
    )
        
    
)
(:action deliver_base_to_station
    :parameters (?r - robot ?m - ringstation ?s - side ?b - base)
    :precondition 
    (and 
        (is_input ?s)
        (robot_with_product ?r ?b)
        (robot_at_machine_side ?r ?m ?s) 
    )
    :effect 
    (and 
        (robot_without_product ?r)          
        (not (robot_with_product ?r ?b))
        (increase (bases_on_ringsation ?m) 1)
    )
)

(:action deliver_make_ring
    :parameters (?r - robot ?m - ringstation ?s - side ?p - product ?target_product - ring ?b - standardbase )
    :precondition 
    (and 
        
        (ringstation_has_product ?m ?target_product)
        (is_input ?s)
        (robot_with_product ?r ?b)
        (machine_without_product ?m)
        (robot_at_machine_side ?r ?m ?s)
        (is_free_above ?target_product)
        (is_free_below ?target_product)
        (is_free_above ?p)
        
        (is_something_below_product ?p)
        (>= (bases_on_ringsation ?m) (cost_of_ring ?target_product))
    )
    :effect 
    (and 
        (machine_with_product ?m ?b )
        (robot_without_product ?r)
        (not (robot_with_product ?r ?b))
            
        (stacked ?target_product ?p ?b)

        
        (is_something_below_product ?target_product)
        (not(is_free_below ?target_product))
        (is_something_above_product ?p)
        (not(is_free_above ?p))

        (decrease (bases_on_ringsation ?m) (cost_of_ring ?target_product)) 
    )
)
(:action deliver_cap
    :parameters (?r - robot ?m - capstation ?s - side ?p - product ?target_product - cap ?b - standardbase)
    :precondition 
    (and       
        (is_input ?s)
        (robot_at_machine_side ?r ?m ?s)
        (robot_with_product ?r ?b)
        (is_free_above ?target_product)
        (is_free_below ?target_product)
        (is_free_above ?p)
        (is_something_below_product ?p)
        (has_capstation_buffer ?m ?target_product)
        (machine_without_product ?m)
    )
    :effect 
    (and 
        (machine_with_product ?m ?b)
        (robot_without_product ?r)
        (not (robot_with_product ?r ?b))
        (stacked ?target_product ?p ?b)

        (not(has_capstation_buffer ?m ?target_product))
        (is_something_below_product ?target_product)
        (not(is_free_below ?target_product))
        (is_something_above_product ?p)
        
        (not(is_free_above ?p))
    )
)

(:action deliver_final_product
    :parameters (?r - robot ?m - deliverstation ?s - side ?p - standardbase)
    :precondition 
    (and 
        (is_input ?s)
        (robot_with_product ?r ?p)
        (robot_at_machine_side ?r ?m ?s)
        (is_something_above_product ?p)
    )
    :effect 
    (and 
        (machine_with_product ?m ?p)
        (robot_without_product ?r)
        (not (robot_with_product ?r ?p))
    )
)

(:action deliver_trash_product
    :parameters (?r - robot ?m - deliverstation ?p - product)
    :precondition 
    (and 
        (robot_with_product ?r ?p)
    )
    :effect 
    (and 
        (robot_without_product ?r)
        (not (robot_with_product ?r ?p))
    )
)

)