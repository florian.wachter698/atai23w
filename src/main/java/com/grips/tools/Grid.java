package com.grips.tools;

import com.shared.domain.Point2dInt;
import lombok.extern.apachecommons.CommonsLog;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@CommonsLog
public class Grid {

    private Node[][] nodes;
    private int width;
    private int height;

    public Grid(int width, int height) {
        this.width = width;
        this.height = height;
        log.info("Creating Grid h: " + height + " w: " + width);
        //by default, all nodes are unoccupied
        nodes = new Node[width][height];
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                nodes[x][y] = new Node(x+1, y+1, false);
            }
        }
    }

    public void setOccupied(Point2dInt point, boolean occupied) {
        if (!isValid(point.getX(), point.getY())) {
            return;
        }
        getNodeAt(point.getX(), point.getY()).setOccupied(occupied);
    }

    public boolean isValid(int x, int y) {
        if (x == 0 || y == 0 || x > width || y > height) {
            return false;
        }

        return true;
    }

    public Node getNodeAt(int x, int y) {
        if (!isValid(x, y)) {
            throw new RuntimeException("Invalid grid Access to x: " + x + " y: " + y);
        }
        return nodes[x-1][y-1];
    }

    public Node getNodeAt(Point2dInt point) {
        if (!isValid(point.getX(), point.getY())) {
            return null;
        }
        return nodes[point.getX()-1][point.getY()-1];
    }

    public boolean isOccupied(int x, int y) {
        if (!isValid(x, y)) {
            return false;
        }

        return getNodeAt(x, y).isOccupied();
    }

    public List<Node> getNeighbors(Node node, boolean diagonalMovement) {
        List<Node> neighbors = new ArrayList<>();

        int x = node.getX();
        int y = node.getY();

        if (isValid(x, y-1) && !isOccupied(x, y-1)) {
            neighbors.add(getNodeAt(x, y-1));
        }

        if (isValid(x+1, y) && !isOccupied(x+1, y)) {
            neighbors.add(getNodeAt(x+1, y));
        }

        if (isValid(x, y+1) && !isOccupied(x, y+1)) {
            neighbors.add(getNodeAt(x, y+1));
        }

        if (isValid(x-1, y) && !isOccupied(x-1, y)) {
            neighbors.add(getNodeAt(x-1, y));
        }

        if (diagonalMovement == true) {
            if (isValid(x-1, y-1) && !isOccupied(x-1, y-1)) {
                neighbors.add(getNodeAt(x-1, y-1));
            }

            if (isValid(x+1, y-1) && !isOccupied(x+1, y-1)) {
                neighbors.add(getNodeAt(x+1, y-1));
            }

            if (isValid(x+1, y+1) && !isOccupied(x+1, y+1)) {
                neighbors.add(getNodeAt(x+1, y+1));
            }

            if (isValid(x-1, y+1) && !isOccupied(x-1, y+1)) {
                neighbors.add(getNodeAt(x-1, y+1));
            }
        }

        return neighbors;
    }

    public void printGridWithPath(LinkedList<Node> path) {

        for (int y = 1; y <= height; y++) {
            System.out.print("|");
            for (int x = 1; x <= width; x++) {
                final int fx = x;
                final int fy = y;
                Optional<Node> onPath = path.stream().filter(n -> (n.getX() == fx && n.getY() == fy)).findAny();

                if (onPath.isPresent()) {
                    System.out.print("p|");
                } else {
                    System.out.print(getNodeAt(x, y).isOccupied() ? "x|" : " |");
                }
            }
            System.out.println();
        }
    }

    public void resetPathCosts() {
        for (int x = 1; x <= width; ++x) {
            for (int y = 1; y <= height; ++y) {
                getNodeAt(x, y).resetCosts();
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
