package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigInteger;

@Getter
@Setter
@ToString
@Configuration
@ConfigurationProperties(prefix = "exploration")
public class ExplorationConfig {
    private boolean early_cap_buffering;
    private BigInteger start_early_cap_buffering;
    private TimeOffset time_offset;
}

