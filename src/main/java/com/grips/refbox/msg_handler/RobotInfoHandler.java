package com.grips.refbox.msg_handler;

import com.rcll.domain.Peer;
import com.rcll.domain.PeerState;
import com.rcll.protobuf_lib.RobotConnections;
import com.robot_communication.services.GripsRobotClient;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.RobotInfoProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@CommonsLog
@Service
public class RobotInfoHandler implements Consumer<RobotInfoProtos.RobotInfo> {

    private final RobotConnections robotConnections;
    private final GripsRobotClient robotClient;

    public RobotInfoHandler(RobotConnections robotConnections,
                            GripsRobotClient robotClient) {
        this.robotConnections = robotConnections;
        this.robotClient = robotClient;
    }


    @Override
    public void accept(RobotInfoProtos.RobotInfo robotInfo) {
        for (RobotInfoProtos.Robot r : robotInfo.getRobotsList()) {
            int robotId = r.getNumber();

            if (!r.getTeam().equalsIgnoreCase("GRIPS")) {
                continue;
            }

            Peer robot = robotConnections.getRobot((long) robotId);

            if (robot == null) {
                return;
            }

            PeerState oldRobotState = robot.getRobotState();

            if (robot != null) {
                if (r.getState().equals(RobotInfoProtos.RobotState.ACTIVE)) {
                    robot.setRobotState(PeerState.ACTIVE);
                } else if (r.getState().equals(RobotInfoProtos.RobotState.MAINTENANCE)) {
                    robot.setRobotState(PeerState.MAINTENANCE);
                } else if (r.getState().equals(RobotInfoProtos.RobotState.DISQUALIFIED)) {
                    robot.setRobotState(PeerState.DISQUALIFIED);
                } else {
                    log.error("Unknown robotstate " + r.getState().toString() + " received for robot " + r.getNumber());
                }
            }

            if (PeerState.ACTIVE.equals(oldRobotState)
                    && (PeerState.MAINTENANCE.equals(robot.getRobotState()) || (PeerState.DISQUALIFIED.equals(robot.getRobotState())))) {

                // if we go from active to maintenance or disqualified, cancel task at robot and fail
                // task at teamserver
                robotClient.cancelTask(new Long(robot.getId()).intValue());
                //productionScheduler.failRobotTasks(new Long(robot.getId()).intValue());
            }
        }
    }
}
