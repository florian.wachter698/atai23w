package com.grips.scheduler.asp;

import com.grips.config.ProductionConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.Atom;
import com.grips.persistence.domain.WorldKnowledge;
import com.rcll.domain.Order;
import com.rcll.domain.ProductComplexity;
import com.rcll.domain.RingColor;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.util.TempAtom;
import com.rcll.refbox.RefboxClient;
import fr.uga.pddl4j.util.IntExp;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

@CommonsLog
@Service
public class KnowledgeBase {

    private final AtomDao atomDao;
    @Value("${gameconfig.planningparameters.numberofrobots}")
    private int numberRobots;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private RcllCodedProblem cp;


    @Autowired
    private ProductionConfig productionConfig;

    @Value("${gameconfig.productiontimes.normalTask}")
    private int normalTaskDuration;
    @Value("${gameconfig.productiontimes.bufferTask}")
    private int bufferTaskDuration;
    @Value("${gameconfig.productiontimes.moveTask}")
    private int moveTaskDuration;

    private final RefboxClient refboxClient;
    public KnowledgeBase(AtomDao atomDao, MachineInfoRefBoxDao machineInfoRefBoxDao, ProductionConfig productionConfig,
                          RefboxClient refboxClient) {
        this.atomDao = atomDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.productionConfig = productionConfig;
        this.refboxClient = refboxClient;
    }

    //ATAI: define the initial atoms of the KB here
    public void generateInitialKB () {
        //generation of standard atom (robots, stations,  ecc...):
        this.add(new Atom("robot_at_machine_side" ,"robot1;startmachine;start1"));
        this.add(new Atom("robot_at_machine_side" ,"robot2;startmachine;start2"));
        this.add(new Atom("robot_at_machine_side" ,"robot3;startmachine;start3"));
        
        //BaseMachine
        this.add(new Atom("side_connected_to_machine","basemachine;output_basemachine"));
        this.add(new Atom("side_connected_to_machine","basemachine;input_basemachine"));
        this.add(new Atom("machine_with_product","basemachine ;redbase1"));
        this.add(new Atom("machine_with_product","basemachine;redbase2"));
        this.add(new Atom("machine_with_product","basemachine;blackbase1"));
        this.add(new Atom("machine_with_product","basemachine;blackbase2"));

        //Ringmachine 1 and 2
        this.add(new Atom("side_connected_to_machine","ringmachine1;output_ringmachine1"));
        this.add(new Atom("side_connected_to_machine","ringmachine1;input_ringmachine1"));
        this.add(new Atom("machin_without_product","ringmachine1"));

        this.add(new Atom("ringstation_has_product","ringmachine1;greenring1"));
        this.add(new Atom("ringstation_has_product","ringmachine1;bluering1"));
        
        this.add(new Atom("side_connected_to_machine","ringmachine2;output_ringmachine2"));
        this.add(new Atom("side_connected_to_machine","ringmachine2;input_ringmachine2"));
        this.add(new Atom("machin_without_product","ringmachine2"));

        this.add(new Atom("ringstation_has_product","ringmachine2;yellowring1"));
        this.add(new Atom("ringstation_has_product","ringmachine2;orangering1"));

        
        //Capstation 1 and 2
        this.add(new Atom("side_connected_to_machine","capmachine1;output_capmachine1"));
        this.add(new Atom("side_connected_to_machine","capmachine1;input_capmachine1"));
        this.add(new Atom("machin_without_product","capmachine1"));

        this.add(new Atom("side_connected_to_machine","capmachine2;output_capmachine2"));
        this.add(new Atom("side_connected_to_machine","capmachine2;input_capmachine2"));
        this.add(new Atom("machin_without_product","capmachine2"));
       
        
        this.add(new Atom("capstation_has_product","capmachine1;blackcap1"));
        this.add(new Atom("capstation_has_product","capmachine1;blackcap2"));
        this.add(new Atom("capstation_has_product","capmachine2;greycap1"));
        this.add(new Atom("capstation_has_product","capmachine2;greycap2"));

        //Deliverymachine
        this.add(new Atom("side_connected_to_machine","delivermachine;output_delivermachine"));
        this.add(new Atom("side_connected_to_machine","delivermachine;input_delivermachine"));
        this.add(new Atom("machin_without_product","delivermachine"));

        //RINGS 
        this.add(new Atom("cost_of_ring","yellowring1",2));
        this.add(new Atom("cost_of_ring","redring1",1));
        this.add(new Atom("cost_of_ring","bluering1",1));
        this.add(new Atom("cost_of_ring","greenring1",0));

        //INPUTS & OUTPUTS
        this.add(new Atom("is_input","input_basemachine"));
        this.add(new Atom("is_input","input_ringmachine1"));
        this.add(new Atom("is_input","input_ringmachine2"));
        this.add(new Atom("is_input","input_capmachine1"));
        this.add(new Atom("is_input","input_capmachine2"));
        this.add(new Atom("is_input","input_delivermachine"));
      
        this.add(new Atom("is_output","output_basemachine"));
        this.add(new Atom("is_output","output_ringmachine1"));
        this.add(new Atom("is_output","output_ringmachine2"));
        this.add(new Atom("is_output","output_capmachine1"));
        this.add(new Atom("is_output","output_capmachine2"));
        this.add(new Atom("is_output","output_delivermachine"));


        this.add(new Atom("is_not_robot_at_side","input_basemachine"));
        this.add(new Atom("is_not_robot_at_side","input_ringmachine1"));
        this.add(new Atom("is_not_robot_at_side","input_ringmachine2"));
        this.add(new Atom("is_not_robot_at_side","input_capmachine1"));
        this.add(new Atom("is_not_robot_at_side","input_capmachine2"));
        this.add(new Atom("is_not_robot_at_side","input_delivermachine"));
        this.add(new Atom("is_not_robot_at_side","output_basemachine"));
        this.add(new Atom("is_not_robot_at_side","output_ringmachine1"));
        this.add(new Atom("is_not_robot_at_side","output_ringmachine2"));
        this.add(new Atom("is_not_robot_at_side","output_capmachine1"));
        this.add(new Atom("is_not_robot_at_side","output_capmachine2"));
        this.add(new Atom("is_not_robot_at_side","output_delivermachine"));

        //DEFINE PRODUCT STRUCTURE
        this.add(new Atom("is_free_above","yellowring1"));
        this.add(new Atom("is_free_above","bluering1"));
        this.add(new Atom("is_free_above","greenring1"));
        this.add(new Atom("is_free_above","orangering1"));
        this.add(new Atom("is_free_above","redbase1"));
        this.add(new Atom("is_free_above","redbase2"));
        this.add(new Atom("is_free_above","blackbase1"));
        this.add(new Atom("is_free_above","blackbase2"));
        this.add(new Atom("is_free_above","blackcap1"));
        this.add(new Atom("is_free_above","blackcap2"));
        this.add(new Atom("is_free_above","greycap1"));
        this.add(new Atom("is_free_above","greycap2"));
        

        this.add(new Atom("is_free_below","yellowring1"));
        this.add(new Atom("is_free_below","bluering1"));
        this.add(new Atom("is_free_below","greenring1"));
        this.add(new Atom("is_free_below","orangering1"));
        this.add(new Atom("is_something_below_product","redbase1"));
        this.add(new Atom("is_something_below_product","redbase2"));
        this.add(new Atom("is_something_below_product","blackbase1"));
        this.add(new Atom("is_something_below_product","blackbase2"));
        this.add(new Atom("is_free_below","blackcap1"));
        this.add(new Atom("is_free_below","blackcap2"));
        this.add(new Atom("is_free_below","greycap1"));
        this.add(new Atom("is_free_below","greycap2"));

        //CAP & BASES
        this.add(new Atom("cap_and_base_connected","blackcap1;cb1"));
        this.add(new Atom("cap_and_base_connected","blackcap2;cb2"));
        this.add(new Atom("cap_and_base_connected","greycap1;cb4"));
        this.add(new Atom("cap_and_base_connected","greycap2;cb5"));

    
    }

    private String ringToColorInPddl(RingColor ringColor) {
        switch (ringColor) {
            case Blue:
                return "ring_blue";
            case Green:
                return "ring_green";
            case Orange:
                return "ring_orange";
            case Yellow:
                return "ring_yellow";
        }
        throw new IllegalArgumentException("Unkown ring color: " + ringColor);
    }

    //ATAI: given a set of goal chosen by the goalreasoner, model the related KB atoms here
    public void generateOrderKb (List<Order> goals) {
        for (Order p : goals) {
            long id = p.getId();
            this.add(new Atom("stage_c0_0", "p" + id));

        }
    }


    public void printKB () {
        for( Atom at : atomDao.findAll())
            System.out.println(at.toString());
    }


    public void prettyPrintKBtoProblemFile (FileWriter fr) throws IOException {
        for (Atom at : atomDao.findAll())
            if(!at.getName().equals("holdafter") )
                fr.write(at.prettyPrinter());
    }


    public void add (Atom at) {
        if (atomDao.findByNameAndAttributes(at.getName(), at.getAttributes()).size() == 0 )
            atomDao.save(at);
        else
            log.warn("Trying to add to KB atom " + at.toString() + " already present");

    }



    public boolean isNotInKB (String pd, String attributes) {
        return atomDao.findByNameAndAttributes(pd, attributes).isEmpty();
    }

    public void setCp (RcllCodedProblem cp) {
        this.cp = cp;
    }


    public String intToString (int number) {
        switch (number) {

            case 0:
                return "zero";

            case 1:
                return "one";

            case 2:
                return "two";

            case 3:
                return "three";

            default:
                return null;

        }
    }


    public void purgeKB () {
        atomDao.deleteAll();
    }







    /**
     * check if end precondition of action are satisfied
     * @param action
     * @return
     */



}
