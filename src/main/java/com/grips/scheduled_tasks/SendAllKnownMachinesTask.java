/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.scheduled_tasks;

import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.rcll.protobuf_lib.RobotConnections;
import com.robot_communication.services.GripsRobotClient;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.ZoneProtos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@CommonsLog
public class SendAllKnownMachinesTask {

    @Autowired
    private RobotConnections robotConnections;

    @Autowired
    private GripsRobotClient robotClient;

    @Autowired
    private MachineInfoRefBoxDao machineInfoRefBoxDao;

    @Scheduled(cron = "${task.sendAllKnownMachines}")
    public void run() {
        robotConnections.getclientId().forEach(this::sendAllKnownMachines);
    }

    public void sendAllKnownMachines(Long robotId) {
        List<MachineInfoProtos.Machine> machines = new ArrayList<>();
        for (MachineInfoRefBox mirb : machineInfoRefBoxDao.findAll()) {
            MachineInfoProtos.Machine m = MachineInfoProtos.Machine.newBuilder()
                    .setName(mirb.getName())
                    .setZone(ZoneProtos.Zone.valueOf(mirb.getZone()))
                    .setRotation(mirb.getRotation()).build();
            machines.add(m);
        }

        MachineInfoProtos.MachineInfo mi = MachineInfoProtos.MachineInfo.newBuilder().addAllMachines(machines).build();
        robotClient.sendToRobot(robotId, mi);
    }
}
